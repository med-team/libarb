#!/usr/bin/make -f
# debian/rules for arb using quilt

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/default.mk
libdevel=$(DEB_SOURCE)-dev

ARBHOME=$(CURDIR)

BITARCH := $(shell dpkg-architecture -qDEB_BUILD_ARCH_BITS)
ifeq ($(BITARCH),64)
    ARB_64:=1
else
    ARB_64:=0
endif

LD_LIBRARY_PATH := $(ARBHOME)/lib:$(LD_LIBRARY_PATH)
LC_ALL := C

export ARBHOME LD_LIBRARY_PATH PATH LC_ALL
export DEB_CXXFLAGS_MAINT_APPEND=-std=c++98

%:
	dh $@

config.makefile: config.makefile.template
	# ARB's build system is configured with config.makefile. This file is
	# created on the first run of make from config.makefile.template. We
	# just use sed here to set the desired parameters:
	sed -e 's/DEVELOPER := ANY/DEVELOPER := RELEASE/;'\
            -e 's/ARB_64 := 1/ARB_64 := $(ARB_64)/;'\
            -e 's/# DEBIAN := 1/DEBIAN := 1/;' \
             config.makefile.template > config.makefile

override_dh_auto_build:
	ln -s TEMPLATES INCLUDE
	for header in CORE/*.h UNIT_TESTER/*.h SL/CB/*.h SL/PTCLEAN/*.h WINDOW/*.hxx ARBDB/*.h AISC_COM/C/*.h SERVERCNTRL/*.h ; \
	    do ln -s ../$${header} INCLUDE/`basename $${header}` ; \
	done
	ln -s /usr/include/valgrind/valgrind.h INCLUDE/valgrind.h
	ln -s ../AISC_COM/AISC PROBE_COM/AISC
	ln -s ../AISC_COM/C PROBE_COM/C
	ln -s ../PROBE_COM/PT_com.h INCLUDE/PT_com.h
	ln -s ../PROBE_COM/PT_server.h INCLUDE/PT_server.h
	ln -s ../PROBE_COM/PT_server_prototypes.h INCLUDE/PT_server_prototypes.h
	ln -s ../SL/HELIX/BI_basepos.hxx INCLUDE/BI_basepos.hxx
	ln -s ../SL/HELIX/BI_helix.hxx INCLUDE/BI_helix.hxx
	mkdir bin
	dh_auto_build -- all # --sourcedirectory=CORE
	make bin/arb_pt_server
	cd lib && make tcpdat

override_dh_auto_configure: config.makefile

override_dh_auto_clean: config.makefile
	# config.makefile is required to run make clean, hence the dependency
	# on ..._auto_configure.
	if [ ! -e config.makefile ] ; then echo "config.makefile missing" ; exit 1 ; fi
	ln -s ../AISC_COM/AISC PROBE_COM/AISC
	$(MAKE) clean
	cd PROBE_COM; $(MAKE) clean; rm -f PT_*.h
	rm -rf bin
	rm -rf lib/lib*
	rm -rf NAMES_COM
	rm -rf PROBE_COM/AISC
	rm -rf INCLUDE
	rm -rf AISC/aisc
	rm -rf AISC_MKPTPS/aisc_mkpt
	rm -rf PROBE_COM/DUMP PROBE_COM/GENC PROBE_COM/GENH
	rm config.makefile
	rm -rf lib/arb_tcp.dat
	# ARB does not have "distclean" or "realclean". Remove some leftovers:
	rm -f UNIT_TESTER/Makefile.setup.local
	find -name \*.log -a ! -name phyml-manual.log -print0 | xargs -0 rm -f
	rm -f TEMPLATES/arb_build.h

override_dh_install:
	dh_install
	mkdir -p debian/$(libdevel)/usr/include/arb
	install INCLUDE/*.h INCLUDE/*.hxx debian/$(libdevel)/usr/include/arb
	rm -rf debian/$(libdevel)/usr/include/arb/valgrind.h

override_dh_strip:
	# Fix rpath issue (once libarb is installed)
	if [ -e debian/libarb/usr/lib/arb/lib/ARB.so ] ; then \
	    chrpath --delete debian/libarb/usr/lib/arb/lib/ARB.so ; \
	fi
	dh_strip
	find debian/libarb-dev -name \*.a -print0 | \
	  xargs -0 -n 1 strip --strip-unneeded \
	                      --remove-section=.comment

override_dh_installchangelogs:
	dh_installchangelogs arb_CHANGES.txt
